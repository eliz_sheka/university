<?php
declare(strict_types = 1);

namespace App\Providers;

use Illuminate\{
    Support\ServiceProvider,
    Foundation\AliasLoader
};

/**
 * Class DevEnvServiceProvider
 * @package App\Providers
 */
class DevEnvServiceProvider extends ServiceProvider
{
    /**
     * List of Local Environment Providers
     *
     * @var array
     */
    protected $localProviders = [];


    /**
     * List of only Local Environment Facade Aliases
     * @var array
     */
    protected $facadeAliases = [
        'Debugbar' => \Barryvdh\Debugbar\Facade::class,
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function register(): void
    {
        if (! $this->app->environment() !== 'production') {
            $this->registerServiceProviders();
            $this->registerFacadeAliases();
        }
    }

    /**
     * Load local service providers
     */
    protected function registerServiceProviders(): void
    {
        foreach ($this->localProviders as $provider) {
            $this->app->register($provider);
        }
    }

    /**
     * Load additional Aliases
     */
    public function registerFacadeAliases(): void
    {
        $loader = AliasLoader::getInstance();
        foreach ($this->facadeAliases as $alias => $facade) {
            $loader->alias($alias, $facade);
        }
    }

}
