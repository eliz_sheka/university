<?php

namespace App\Providers;

use App\Models\Achievement;
use App\Models\Gallery;
use App\Models\Graduate;
use App\Models\History;
use App\Models\Link\Link;
use App\Models\Link\LinkCategory;
use App\Models\News;
use App\Models\Teacher\Article;
use App\Models\Teacher\Subject;
use App\Models\Teacher\Teacher;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        // Define models binding
        $this->bindModels();

        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAcpRoutes();

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "acp" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAcpRoutes()
    {
        Route::group([
            'as'         => 'acp',
            'prefix'     => 'acp',
            'namespace'  => $this->namespace . '\\Acp',
            'middleware' => ['web', 'auth', 'role:root|admin'],
        ], function () {
            require_once(base_path('routes/acp.php'));
        });
    }

    /**
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    protected function bindModels()
    {
        $models = [
            'user'     => User::class,
            'news'     => News::class,
            'subject'  => Subject::class,
            'article'  => Article::class,
            'category' => LinkCategory::class,
            'link'     => Link::class,
            'history'  => History::class,
            'photo'    => Gallery::class,
            'graduate' => Graduate::class,
        ];

        foreach ($models as $key => $class) {
            Route::pattern($key, '[0-9]+');
            Route::model($key, $class);
        }

        $slugModels = [
            'teacher'     => Teacher::class,
            'news'        => News::class,
            'achievement' => Achievement::class,
        ];

        foreach ($slugModels as $key => $class) {
            Route::pattern($key, '[a-z0-9-]+');
            Route::model($key, $class);
        }
    }
}
