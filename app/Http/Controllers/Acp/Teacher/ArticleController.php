<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp\Teacher;

use App\Http\Requests\Acp\Teacher\Article\ArticlePositionRequest;
use App\Http\Requests\Acp\Teacher\Article\ArticleRequest;
use App\Models\Teacher\Article;
use App\Models\Teacher\Teacher;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class ArticleController
 *
 * @package App\Http\Controllers\Acp\Teacher
 */
class ArticleController extends Controller
{
    /**
     * @param Teacher $teacher
     *
     * @return View
     */
    public function create(Teacher $teacher): View
    {
        return \view('acp.teachers.articles.create', [
            'teacher' => $teacher,
        ]);
    }

    /**
     * @param string  $paginate
     * @param Teacher $teacher
     *
     * @return JsonResponse
     */
    public function show($paginate = 'paginate', Teacher $teacher): JsonResponse
    {
        $articleRows = [];
        $articlePages = [];
        if ($paginate == 'all') {
            $articles = $teacher->articles()->orderBy('position')->get();
        } else {
            $articles = $teacher->articles()->orderBy('position')->paginate(10);
            $articlePages = [
                'current_page' => (int)$articles->currentPage(),
                'total'        => (int)$articles->total(),
                'per_page'     => (int)$articles->perPage(),
            ];
        }

        foreach ($articles as $i => $article) {
            $item = ($article instanceof Article ? $article : $article[$i]);
            $articleRows[$i] = [
                'id'      => $item->id,
                'article' => ['n' => 'article['.$i.'][description]', 'v' => $item->getAttribute('description')],
                'link'    => ['n' => 'article['.$i.'][link]', 'v' => $item->getAttribute('link')],
            ];
        }

        $data = [$articleRows, $articlePages];

        return response()->json($data);
    }

    /**
     * @param ArticleRequest $request
     * @param Teacher        $teacher
     *
     * @return RedirectResponse
     */
    public function store(ArticleRequest $request, Teacher $teacher): RedirectResponse
    {
        $teacher->articles()->createMany($request->input('article'));

        return \redirect()->route('acp.teacher.show', $teacher->getAttribute('slug'))
            ->with('success', \trans('acp/article.create.form.messages.success'));
    }

    /**
     * @param ArticleRequest $request
     * @param Article        $article
     *
     * @return JsonResponse
     */
    public function update(ArticleRequest $request, Article $article): JsonResponse
    {
        $article->update([
            'description' => $request->get('name'),
            'link'        => $request->get('link'),
        ]);

        return \response()->json(\trans('acp/article.edit.form.messages.success'));
    }


    /**
     * @param ArticlePositionRequest $request
     *
     * @return JsonResponse
     */
    public function updatePosition(ArticlePositionRequest $request): JsonResponse
    {
        foreach ($request->get('data') as $position => $id) {
            Article::find($id)->update([
                'position' => (int)$position,
            ]);
        }

        return \response()->json(\trans('acp/article.edit.form.messages.success'));
    }

    /**
     * @param Article $article
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function destroy(Article $article): JsonResponse
    {
        return (false === $article->delete())
            ? response()->json(\trans('acp/article.delete.messages.failure'))
            : response()->json(\trans('acp/article.delete.messages.success'));
    }
}
