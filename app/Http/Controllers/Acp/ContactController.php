<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Acp;

use App\Http\Requests\Acp\ContactRequest;
use App\Models\Contact;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Class ContactController
 *
 * @package App\Http\Controllers\Acp
 */
class ContactController extends Controller
{
    /**
     * @return View
     */
    public function show(): View
    {
        return \view('acp.contact.index', [
            'contacts' => Contact::first(),
        ]);
    }

    /**
     * @param ContactRequest $request
     * @param Contact        $contact
     *
     * @return RedirectResponse
     */
    public function update(ContactRequest $request, Contact $contact): RedirectResponse
    {
        $contact->update([
            'address' => $request->input('address'),
            'phone'  => $request->input('phone'),
        ]);

        // redirect back to the list
        return \redirect()->route('acp.contact.show')
            ->with('success', \trans('acp/contact.edit.form.messages.success'));

    }
}
