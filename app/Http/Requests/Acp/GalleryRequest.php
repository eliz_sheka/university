<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GalleryRequest
 * 
 * @package App\Http\Requests\Acp
 */
class GalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:4|max:255',
            'photo' => 'nullable|image',
        ];
    }

    /**
     * Custom error messages
     * 
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => \trans('acp/gallery.create.form.fields.title.errors.required'),
            'title.string'   => \trans('acp/gallery.create.form.fields.title.errors.string'),
            'title.min'      => \trans('acp/gallery.create.form.fields.title.errors.min'),
            'title.max'      => \trans('acp/gallery.create.form.fields.title.errors.max'),

            'photo.image'    => \trans('acp/gallery.create.form.fields.photo.errors.image'),
        ];
    }
}
