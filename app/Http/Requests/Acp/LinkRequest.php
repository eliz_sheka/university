<?php

declare(strict_types = 1);

namespace App\Http\Requests\Acp;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LinkRequest
 * 
 * @package App\Http\Requests\Acp
 */
class LinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'sometimes|required|string|min:4|max:255',
            'description' => 'nullable|min:4',
            'category'    => 'required|string|min:4|max:255',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => \trans('acp/link.create.form.fields.title.errors.required'),
            'title.string'   => \trans('acp/link.create.form.fields.title.errors.string'),
            'title.min'      => \trans('acp/link.create.form.fields.title.errors.min'),
            'title.max'      => \trans('acp/link.create.form.fields.title.errors.max'),

            'description.required' => \trans('acp/link.create.form.fields.link.errors.required'),
            'description.string'   => \trans('acp/link.create.form.fields.link.errors.string'),
            'description.min'      => \trans('acp/link.create.form.fields.link.errors.min'),
            'description.max'      => \trans('acp/link.create.form.fields.link.errors.max'),

            'category.required' => \trans('acp/link.create.form.fields.category.errors.required'),
            'category.string'   => \trans('acp/link.create.form.fields.category.errors.string'),
            'category.min'      => \trans('acp/link.create.form.fields.category.errors.min'),
            'category.max'      => \trans('acp/link.create.form.fields.category.errors.max'),
        ];
    }
}
