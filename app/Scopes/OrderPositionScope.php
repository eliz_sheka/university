<?php

declare(strict_types = 1);

namespace App\Scopes;

use Illuminate\Database\Eloquent\{
    Scope, Model, Builder
};

/**
 * Class OrderPositionScope
 * 
 * @package App\Scopes
 */
class OrderPositionScope implements Scope
{
    /**
     * Default order is LIFO
     */
    const DEFAULT_ORDER_BY = 'position';

    /**
     * @var string
     */
    protected $orderBy;

    /**
     * OrderDateScope constructor.
     *
     * @param string $orderBy
     */
    public function __construct(string $orderBy = self::DEFAULT_ORDER_BY)
    {
        $this->orderBy = $orderBy;
    }

    /**
     * Global LIFO order scope
     * 
     * @param  Builder $builder
     * @param  Model   $model
     * 
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $builder->orderBy($this->orderBy);
    }
}