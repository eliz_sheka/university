<?php

declare(strict_types = 1);

namespace App\Models\Teacher;

use App\Scopes\OrderPositionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\MediaLibrary\Media;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\{
    HasMediaTrait,
    Interfaces\HasMedia,
    Interfaces\HasMediaConversions
};

/**
 * Class Teacher
 * 
 * @package App\Models\Teacher
 */
class Teacher extends Model implements HasMedia, HasMediaConversions
{
    use HasMediaTrait;
    
    /**
     * @var string
     */
    protected $table = 'teachers';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'last_name',
        'name',
        'post',
        'biography',
        'email',
        'schedule',
        'position',
        'slug',
    ];

    /**
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'last_name' => 'string',
        'name'      => 'string',
        'post'      => 'string',
        'biography' => 'text',
        'email'     => 'string',
        'schedule'  => 'string',
        'position'  => 'integer',
        'slug'      => 'string',
    ];

    /**
     * @var array
     */
    protected $appends = [
        'full_name',
    ];
    
    /**
     * @return HasMany
     */
    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }

    /**
     * @return HasMany
     */
    public function subjects(): HasMany
    {
        return $this->hasMany(Subject::class);
    }

    /**
     * @return string
     */
    public function getPhotoUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('teachers', 'thumb');
    }

    /**
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 200, 245)
            ->sharpen(10)
            ->performOnCollections('teachers');
    }

    /**
     * Get the teacher's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->last_name} {$this->name}";
    }

    /**
     * Create slug for teacher's pages
     *
     * @return string
     */
    public function createSlug()
    {
        return str_slug($this->full_name, '-');
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * @return void
     * @throws \InvalidArgumentException
     */
    protected static function boot()
    {
        static::addGlobalScope(new OrderPositionScope);
    }
}
