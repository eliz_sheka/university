<?php

declare(strict_types = 1);

namespace App\Models\Teacher;

use App\Scopes\OrderPositionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Subject
 * 
 * @package App\Models\Teacher
 */
class Subject extends Model
{
    /**
     * @var string
     */
    protected $table = 'subjects';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'name',
        'teacher_id',
        'position',
    ];

    /**
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'name'       => 'string',
        'teacher_id' => 'integer',
        'position'   => 'integer',
    ];

    /**
     * @return BelongsTo
     */
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    /**
     * @return void
     * 
     * @throws \InvalidArgumentException
     */
    protected static function boot()
    {
        static::addGlobalScope(new OrderPositionScope);
    }
}
