<?php

declare(strict_types = 1);

namespace App\Models\Link;

use App\Scopes\OrderDateScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

/**
 * Class Link
 *
 * @package App\Models\Link
 */
class Link extends Model implements HasMedia, HasMediaConversions
{
    use HasMediaTrait;

    /**
     * @var string
     */
    protected $table = 'links';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'category_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'title'       => 'string',
        'description' => 'text',
        'category_id' => 'integer',
    ];

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(LinkCategory::class);
    }


    /**
     * @return string
     */
    public function getPhotoUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('info', 'middle');
    }

    /**
     * @param Media|null $media
     *
     * @throws \Spatie\Image\Exceptions\InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('sm')
            ->fit(Manipulations::FIT_CROP, 200, 125)
            ->sharpen(10)
            ->performOnCollections('info_pic');

        $this->addMediaConversion('middle')
            ->fit(Manipulations::FIT_CROP, 400, 250)
            ->sharpen(10)
            ->performOnCollections('info_pic');
    }

    /**
     * @return void
     *
     * @throws \InvalidArgumentException
     */
    protected static function boot()
    {
        static::addGlobalScope(new OrderDateScope);
    }
}
