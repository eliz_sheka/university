<?php

declare(strict_types = 1);

namespace App\Models\Link;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class LinkCategory
 * 
 * @package App\Models\Link
 */
class LinkCategory extends Model
{
    /**
     * @var string
     */
    protected $table = 'link_categories';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'category',
        'icon',
    ];

    /**
     * The attributes that should be cast to native types.
     * 
     * @var array
     */
    protected $casts = [
        'category' => 'string',
        'icon'     => 'string',
    ];

    /**
     * @return HasMany
     */
    public function links(): HasMany
    {
        return $this->hasMany(Link::class, 'category_id');
    }
}
