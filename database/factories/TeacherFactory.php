<?php

declare(strict_types = 1);

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\Teacher\Teacher::class, function (Faker $faker) {
    return [
        'last_name' => $faker->lastName,
        'name' => $faker->firstName.' '.$faker->firstName,
        'email' => $faker->unique()->safeEmail,
        'post' => $faker->jobTitle,
        'biography' => $faker->text(200),
        'schedule' => $faker->time('H:i').'-'.$faker->time('H:i'),
    ];
});
