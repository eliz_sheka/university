<?php

declare(strict_types = 1);

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\Models\News::class, function (Faker $faker) {
    return [
        'title'       => $faker->sentence,
        'description' => $faker->text,
        'author'      => $faker->lastName . ' ' . $faker->firstName,
        'slug'        => str_slug($faker->sentence, '-'),
    ];
});

