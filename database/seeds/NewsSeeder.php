<?php

declare(strict_types = 1);

use Illuminate\Database\Seeder;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Faker\Factory;

/**
 * Class NewsSeeder
 */
class NewsSeeder extends Seeder
{
    use HasMediaTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $news = factory(\App\Models\News::class, 7)->create();

        foreach ($news as $item) {
            $faker = Factory::create();
            for ($i = 0; $i < 7; $i++) {
                $item->addMediaFromUrl($faker->imageUrl())->toMediaCollection('news');
            }
        }
    }
}
