
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('lightbox2/dist/js/lightbox.min');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./components/navbar');
require('./components/alert');
require('./pages/news');
require('./pages/achievement');


const app = new Vue({
    el: '#app',

    mounted() {

        require('./main');
        require('./custom');

        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });
    },

});
