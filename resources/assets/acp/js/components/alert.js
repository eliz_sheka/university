import {vbus} from '../vbus'

Vue.component('alert', {

  template: `
    <div v-if="visible" class="row">
      <div class="alert alert-dismissible" :class="this.severity" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" @click="close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ message }}
      </div>
    </div>
  `,

  data() {
    return {
      message: '',
      visible: false,
      severity: 'alert-success'
    }
  },

  created() {
    vbus.$on('showAlert', function (data) {
      this.message = data.message
      this.severity = 'alert-' + data.severity
      this.visible = true
    }.bind(this))
  },

  mounted() {
    setTimeout(function() {
      this.visible = false
    }.bind(this), 3000)
  },

  methods: {
    close() {
      this.visible = false
    }
  }
})
