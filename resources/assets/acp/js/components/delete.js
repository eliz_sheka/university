import {http} from '../http'
import {vbus} from '../vbus'

Vue.component('delete', {
  props: ['url', 'remove'],

  template: '<a class="pl-10" href="#" @click.prevent="process()"><i class="fa fa-trash"></i></a>',

  methods: {
    process() {
      http.delete(this.url)
        .then(response => {
          $(this.remove).remove()

                vbus.$emit('showAlert',
              { severity: 'success', message: response.data })
        })
        .catch(e => {
          vbus.$emit('showAlert',
              { severity: 'success', message: e.response.data })
        })

    }
  }
})



