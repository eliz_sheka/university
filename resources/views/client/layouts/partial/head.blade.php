<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Title -->
<title>{{ config('app.name') }}</title>
<!-- Favicon -->
<link rel="shortcut icon" href="{{ asset('build/client/images/books.png') }}" type="image/x-icon">
<!-- Styles -->
<link href="{{ mix('build/client/css/app-client.css') }}" rel="stylesheet">
<link href="{{ mix('build/generic/css/font-awesome.css') }}" rel="stylesheet">

<!-- Scripts -->
@push('scripts')
    <script src="{{ asset('build/routes.js') }}"></script>
    <script src="{{ mix('build/client/js/app-client.js') }}"></script>
@endpush
<script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
