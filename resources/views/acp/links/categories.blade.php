@extends('acp.layouts.app')

@section('content')
    @includeIf('acp.component.alert-detector')
    <alert></alert>

    <div id="link-category-panel" class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/link.main.category.title')</strong></h4>
        </div>
        <div class="panel-body">
                @php
                    $linkRows = [];
                    foreach ($categories as $i => $category) {
                        $item = ($category instanceof App\Models\Link\LinkCategory ? $category : $category[$i]);
                        $linkRows[$i] = [
                            'id'       => $item->id,
                            'category' => ['n' => 'category['.$i.']', 'v' => $item->getAttribute('category')],
                            'icon'     => ['n' => 'category['.$i.']', 'v' => $item->getAttribute('icon')],
                        ];
                    }
                @endphp
                <link-category data="{{ json_encode($linkRows) }}" inline-template>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>@lang('acp/link.main.table.name')</th>
                                <th class="text-center">@lang('acp/link.main.table.icon')</th>
                                <th class="text-center">@lang('acp/link.main.table.edit')</th>
                                <th class="text-center">@lang('acp/link.main.table.delete')</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr v-for="row in rows" :id="'category_'+row.id">
                                <td :class="'link-category-block_'+row.id" v-text="row.category.v"></td>
                                <td :class="'link-category-block_'+row.id" class="text-center">
                                    <i :class="'fa fa-'+row.icon.v"></i>
                                </td>
                                <td class="text-center" :class="'link-category-block_'+row.id">
                                    <a href="" @click.prevent="edit(row.id)">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                                <td class="text-center" :class="'link-category-block_'+row.id">
                                    <a href="" @click.prevent="destroy(row.id)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                                <td class="input-group edit-link-category-block"
                                    :id="'edit-link-category-block_'+row.id">
                                    <input type="text" required v-model="row.category.v" :name="row.category.n"
                                           class="form-control">
                                    <input type="text" required v-model="row.icon.v" :name="row.icon.n"
                                           class="form-control"
                                           placeholder="{{ Lang::get('acp/link.main.table.icon') }}">
                                    <span class="input-group-btn">
                                        <button @click.prevent="update(row.category.v, row.icon.v, row.id)"
                                                type="button" class="btn btn-default">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </link-category>
        </div>
    </div>
@endsection
