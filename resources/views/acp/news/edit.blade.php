@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/news.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <form action="{!! route('acp.news.update', $item->slug) !!}" method="post"
                  enctype="multipart/form-data" role="form" class="col-sm-10 col-sm-offset-1">
                <legend>@lang('acp/news.edit.form.fieldsets.title')</legend>

                {!! method_field('patch') !!}
                {!! csrf_field() !!}

                <div class="form-group required {{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="control-label" for="news_title">
                        @lang('acp/news.create.form.fields.title.label')
                    </label>
                    <input required value="{{ $item->title }}" type="text" class="form-control" name="title"
                           id="news_title" placeholder="@lang('acp/news.create.form.fields.title.label')">
                    @includeWhen($errors->has('title'), 'acp.component.field-error', ['field' => 'title'])
                </div>

                <div class="form-group required {{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="control-label" for="news_description">
                        @lang('acp/news.create.form.fields.description.label')
                    </label>
                    <textarea rows="5" class="form-control editor markdown_editor"
                              name="description" id="news_description"
                              placeholder="@lang('acp/news.create.form.fields.description.label')">
                        {{ $item->description }}
                    </textarea>
                    @includeWhen($errors->has('description'), 'acp.component.field-error', ['field' => 'description'])
                </div>

                <div class="form-group {{ $errors->has('author') ? ' has-error' : '' }}">
                    <label class="control-label" for="news_author">
                        @lang('acp/news.create.form.fields.author.label')
                    </label>
                    <input type="text" class="form-control" name="author" id="news_author" value="{{ $item->author }}"
                           placeholder="@lang('acp/news.create.form.fields.author.label')">
                    @includeWhen($errors->has('author'), 'acp.component.field-error', ['field' => 'author'])
                </div>

                <div class="form-group {{ $errors->has('picture') ? ' has-error' : '' }}">
                    <label class="control-label" for="picture">@lang('acp/news.create.form.fields.picture.label')</label>
                    <input type="file" multiple accept="image/jpeg,image/png"
                           class="form-control" name="picture[]" id="picture">
                    @includeWhen($errors->has('picture'), 'acp.component.field-error', ['field' => 'picture'])
                </div>

                <div class="news_image_block edit col-md-12">
                    @forelse($item->getMedia('news') as $pic)
                        <div id="news_{{ $pic->getKey() }}">
                            <img src="{{ $pic->getUrl('sm') }}" alt="">
                            <delete url="{!! route('acp.news.pic.destroy', $pic->getKey()) !!}"
                                    remove="#news_{{ $pic->getKey() }}"></delete>
                        </div>
                    @empty
                    @endforelse
                </div>

                <button type="submit" class="btn btn-primary pull-right">
                    @lang('acp/news.create.form.buttons.submit.text')
                </button>
            </form>
        </div>
    </div>
@endsection
