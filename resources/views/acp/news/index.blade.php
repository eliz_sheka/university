@extends('acp.layouts.app')

@section('content')
    @includeIf('acp.component.alert-detector')
    <alert></alert>

    <div class="panel panel-default acp-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>@lang('acp/news.main.title')</strong></h4>
        </div>
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>@lang('acp/news.main.table.name')</th>
                    <th class="text-center">@lang('acp/news.main.table.date')</th>
                    <th class="text-center">@lang('acp/news.main.table.edit')</th>
                    <th class="text-center">@lang('acp/news.main.table.delete')</th>
                </tr>
                </thead>
                <tbody>
                @forelse($news as $item)
                <tr id="news_{{ $item->getKey() }}">
                    <td><a href="{!! route('acp.news.show', $item->slug) !!}">{{ $item->title }}</a></td>
                    <td class="text-center">{{$item->created_at->format('H:i/d.m.Y')}}</td>
                    <td class="text-center"><a href="{!! route('acp.news.edit', $item->slug) !!}">
                            <i class="fa fa-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center"><delete url="{!! route('acp.news.destroy', $item->slug) !!}"
                                                    remove="#news_{{ $item->getKey() }}"></delete></td>
                </tr>
                @empty
                    <tr>
                        <td>Ще немає новин</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {{ $news->links() }}
        </div>
    </div>
@endsection
