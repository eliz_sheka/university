<div class="help-block">
    <ul class="list-group-item-danger">
        @foreach($errors->get($field) as $message)
            <li>{{ $message }}</li>
        @endforeach
    </ul>
</div>
