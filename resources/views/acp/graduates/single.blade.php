@extends('acp.layouts.app')

@section('content')
    <div class="panel panel-default acp-panel single-teacher-panel">
        <div class="panel-heading acp-panel-heading">
            <h4 class=""><strong>{{ $graduate->name }}</strong></h4>
            <div>
                <a href="{!! route('acp.graduate.edit', $graduate->getKey()) !!}" type="button"
                   class="btn btn-primary">@lang('acp/graduate.edit.form.fieldsets.title')</a>
            </div>
        </div>
        <div class="panel-body">
            <div class="single-teacher">
                <div>
                    <img src="{{ $graduate->getPhotoUrlAttribute('thumb') }}">
                </div>
                <div>
                    <h5 class="">{{ $graduate->subheader }}</h5>
                    {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($graduate->text) !!}
                </div>
            </div>
        </div>
    </div>
@endsection
