<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('acp.layouts.partial.head')
</head>
<body>
<div id="app" v-cloak>
    @include('acp.layouts.partial.navbar')
    @if(Auth::check())
        <div class="row" style="margin-right: 0;">
            <div class="col-sm-3">
                @include('acp.layouts.partial.sidebar')
            </div>
            <div class="col-sm-9">
                @yield('content')
            </div>
        </div>
    @else
        @yield('content')
    @endif
</div>
<!-- Scripts -->
@stack('scripts')
</body>
</html>
