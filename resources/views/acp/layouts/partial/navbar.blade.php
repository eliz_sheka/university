<navbar inline-template>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header" style="margin-left: 50px">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="{!! \route('index') !!}">
                <small>@lang('client/index.header.left')</small>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse" style="margin-right: 50px">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav"></ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @auth
                <li>
                    <a href="{!! route('logout') !!}">@lang('acp/common.navbar.logout') <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                </li>
                @endauth
            </ul>
        </div>
    </nav>
</navbar>
