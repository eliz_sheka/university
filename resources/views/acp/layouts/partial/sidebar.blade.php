<div class="panel-group sidebar" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default ">
        <div class="panel-heading acp-panel-heading @if(app('request')->is('acp/news*')) active-border @endif" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <h4 class="panel-title @if(app('request')->is('acp/news*')) active-color @endif">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    @lang('acp/common.menu.news')
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <a href="{!! route('acp.news.index') !!}" class="list-group-item">
                <i class=" fa fa-list-ul" aria-hidden="true"></i>
                @lang('acp/common.menu.common.watch')
            </a>
            <a href="{!! route('acp.news.create') !!}" class="list-group-item">
                <i class="fa fa-plus" aria-hidden="true"></i>
                @lang('acp/common.menu.common.add')
            </a>
        </div>
    </div>
    <div class="panel panel-default ">
        <div class="panel-heading acp-panel-heading @if(app('request')->is('acp/gallery*')) active-border @endif" role="tab" id="headingTwo"  data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <h4 class="panel-title @if(app('request')->is('acp/gallery*')) active-color @endif">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <i class="fa fa-picture-o" aria-hidden="true"></i>
                    @lang('acp/common.menu.gallery')
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <a href="{!! route('acp.gallery.index') !!}" class="list-group-item">
                <i class=" fa fa-list-ul" aria-hidden="true"></i>
                @lang('acp/common.menu.common.watch')
            </a>
            <a href="{!! route('acp.gallery.create') !!}" class="list-group-item">
                <i class="fa fa-plus" aria-hidden="true"></i>
                @lang('acp/common.menu.common.add')
            </a>
        </div>
    </div>
    <div class="panel panel-default ">
        <div class="panel-heading acp-panel-heading @if(app('request')->is('acp/links*')) active-border @endif" role="tab" id="headingTwo"  data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
            <h4 class="panel-title @if(app('request')->is('acp/links*')) active-color @endif">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    <i class="fa fa-external-link" aria-hidden="true"></i>
                    @lang('acp/common.menu.links')
                </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <a href="{!! route('acp.link.index') !!}" class="list-group-item">
                <i class=" fa fa-list-ul" aria-hidden="true"></i>
                @lang('acp/common.menu.common.watch')
            </a>
            <a href="{!! route('acp.link.create') !!}" class="list-group-item">
                <i class="fa fa-plus" aria-hidden="true"></i>
                @lang('acp/common.menu.common.add')
            </a>
            @if(!\blank(\App\Models\Link\LinkCategory::all()))
            <a href="{!! route('acp.link.category.show') !!}" class="list-group-item">
                <i class="fa fa-th-large" aria-hidden="true"></i>
                @lang('acp/common.menu.categories')
            </a>
            @endif
        </div>
    </div>
    <div class="panel panel-default ">
        <div class="panel-heading acp-panel-heading @if(app('request')->is('acp/teachers*')) active-border @endif" role="tab" id="headingThree" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <h4 class="panel-title @if(app('request')->is('acp/teachers*')) active-color @endif">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    <i class="fa fa-briefcase" aria-hidden="true"></i>
                    @lang('acp/common.menu.teachers')
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <a href="{!! route('acp.teacher.index') !!}" class="list-group-item">
                <i class=" fa fa-list-ul" aria-hidden="true"></i>
                @lang('acp/common.menu.common.watch')
            </a>
            <a href="{!! route('acp.teacher.create') !!}" class="list-group-item">
                <i class="fa fa-plus" aria-hidden="true"></i>
                @lang('acp/common.menu.common.add')
            </a>
        </div>
    </div>
    <div class="panel panel-default ">
        <div class="panel-heading acp-panel-heading @if(app('request')->is('acp/graduate*')) active-border @endif" role="tab" id="headingGraduate" data-toggle="collapse" data-parent="#accordion" href="#collapseGraduate" aria-expanded="false" aria-controls="collapseGraduate">
            <h4 class="panel-title @if(app('request')->is('acp/graduate*')) active-color @endif">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseGraduate" aria-expanded="false" aria-controls="collapseGraduate">
                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    @lang('acp/common.menu.graduates')
                </a>
            </h4>
        </div>
        <div id="collapseGraduate" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingGraduate">
            <a href="{!! route('acp.graduate.index') !!}" class="list-group-item">
                <i class=" fa fa-list-ul" aria-hidden="true"></i>
                @lang('acp/common.menu.common.watch')
            </a>
            <a href="{!! route('acp.graduate.create') !!}" class="list-group-item">
                <i class="fa fa-plus" aria-hidden="true"></i>
                @lang('acp/common.menu.common.add')
            </a>
        </div>
    </div>
    <div class="panel panel-default ">
        <a href="{!! route('acp.history.index') !!}" class="single-link">
            <div class="panel-heading acp-panel-heading @if(app('request')->is('acp/history*')) active-border @endif" role="tab" id="headingFour" >
                <h4 class="panel-title @if(app('request')->is('acp/history*')) active-color @endif">
                    <i class="fa fa-university" aria-hidden="true"></i>
                    @lang('acp/common.menu.about')
                </h4>
            </div>
        </a>
    </div>
    <div class="panel panel-default ">
        <a class="single-link" href="{!! route('acp.contact.show') !!}">
            <div class="panel-heading acp-panel-heading @if(app('request')->is('acp/contacts*')) active-border @endif" role="tab" id="headingFour" >
                <h4 class="panel-title @if(app('request')->is('acp/contacts*')) active-color @endif">
                    <i class="fa fa-address-card-o" aria-hidden="true"></i>
                    @lang('acp/common.menu.contacts')
                </h4>
            </div>
        </a>
    </div>
    <div class="panel panel-default ">
        <a class="single-link" href="{!! route('acp.profile.show') !!}">
            <div class="panel-heading acp-panel-heading @if(app('request')->is('acp/profile*')) active-border @endif" role="tab" id="headingFour" >
                <h4 class="panel-title @if(app('request')->is('acp/profile*')) active-color @endif">
                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                    @lang('acp/common.menu.profile')
                </h4>
            </div>
        </a>
    </div>
</div>
