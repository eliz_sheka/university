<?php

declare(strict_types=1);

return [
    'main'   => [
        'title' => 'Історія',
    ],
    'create' => [
        'form' => [
            'fieldsets' => [
                'title' => 'Створити історію',
            ],
            'fields'    => [
                'title'       => [
                    'label'  => 'Заголовок',
                    'errors' => [
                        'required' => 'Обовʼязкове поле',
                        'string'   => 'Назва повинна бути рядком',
                        'min'      => 'Назва повинна мати більше, ніж :min символи',
                        'max'      => 'Назва не повинна мати більше, ніж :max символів',
                    ],
                ],
                'description' => [
                    'label'  => 'Текст',
                    'errors' => [
                        'required' => 'Обовʼязкове поле',
                    ],
                ],
                'picture'     => [
                    'label'  => 'Зображення',
                    'errors' => [
                        'image' => 'Файл повинен бути зображенням',
                    ],
                ],
            ],
            'buttons'   => [
                'submit' => [
                    'text' => 'Зберегти',
                ],
            ],
            'messages'  => [
                'success' => 'Новина успішно збережена',
                'failure' => 'Сталася помилка під час збереження',
            ],
        ],
    ],
    'edit'   => [
        'form' => [
            'fieldsets' => [
                'title' => 'Редагувати історію',
            ],
            'fields'    => [
                'title'       => [
                    'label'  => 'Заголовок',
                    'errors' => [],
                ],
                'description' => [
                    'label'  => 'Текст',
                    'errors' => [],
                ],
                'picture'     => [
                    'label'  => 'Зображення',
                    'errors' => [
                        'image' => 'Файл повинен бути зображенням',
                    ],
                ],
            ],
            'buttons'   => [
                'submit' => [
                    'text' => 'Зберегти',
                ],
            ],
            'messages'  => [
                'success' => 'Успішно відредаговано',
                'failure' => 'Сталася помилка під час оновлення',
            ],
        ],
    ],
    'delete' => [
        'messages' => [
            'success' => 'Успішно видалено',
            'failure' => 'Сталася помилка під час видалення',
        ],
    ],
];
