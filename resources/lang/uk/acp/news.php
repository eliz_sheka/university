<?php

declare(strict_types=1);

return [
    'main'   => [
        'title' => 'Новини',
        'table' => [
            'name'   => 'Заголовок',
            'date'   => 'Дата',
            'edit'   => 'Редагувати',
            'delete' => 'Видалити',
        ],
    ],
    'create' => [
        'form' => [
            'fieldsets' => [
                'title' => 'Створити новину',
            ],
            'fields'    => [
                'title'       => [
                    'label'  => 'Заголовок',
                    'errors' => [
                        'required' => 'Обовʼязкове поле',
                        'string'   => 'Назва повинна бути рядком',
                        'min'      => 'Назва повинна мати більше, ніж :min символи',
                        'max'      => 'Назва не повинна мати більше, ніж :max символів',
                    ],
                ],
                'author'      => [
                    'label'  => 'Автор',
                    'errors' => [
                        'string' => 'Назва повинна бути рядком',
                        'min'    => 'Назва повинна мати більше, ніж :min символи',
                        'max'    => 'Назва не повинна мати більше, ніж :max символів',
                    ],
                ],
                'description' => [
                    'label'  => 'Текст',
                    'errors' => [
                        'required' => 'Обовʼязкове поле',
                    ],
                ],
                'picture'     => [
                    'label'  => 'Зображення',
                    'errors' => [
                        'image' => 'Файл повинен бути зображенням',
                    ],
                ],
            ],
            'buttons'   => [
                'submit' => [
                    'text' => 'Зберегти',
                ],
            ],
            'messages'  => [
                'success' => 'Новина успішно збережена',
                'failure' => 'Сталася помилка під час збереження',
            ],
        ],
    ],
    'edit'   => [
        'form' => [
            'fieldsets' => [
                'title' => 'Редагувати новину',
            ],
            'fields'    => [
                'title'       => [
                    'label'  => 'Заголовок',
                    'errors' => [],
                ],
                'author'      => [
                    'label'  => 'Автор',
                    'errors' => [
                        'string' => 'Назва повинна бути рядком',
                        'min'    => 'Назва повинна мати більше, ніж :min символи',
                        'max'    => 'Назва не повинна мати більше, ніж :max символів',
                    ],
                ],
                'description' => [
                    'label'  => 'Текст',
                    'errors' => [],
                ],
                'picture'     => [
                    'label'  => 'Зображення',
                    'errors' => [
                        'image' => 'Файл повинен бути зображенням',
                    ],
                ],
            ],
            'buttons'   => [
                'submit' => [
                    'text' => 'Зберегти',
                ],
            ],
            'messages'  => [
                'success' => 'Новина успішно відредагована',
                'failure' => 'Сталася помилка під час оновлення',
            ],
        ],
    ],
    'delete' => [
        'messages' => [
            'success' => 'Новина успішно видалена',
            'failure' => 'Сталася помилка під час видалення',
        ],
    ],
];
