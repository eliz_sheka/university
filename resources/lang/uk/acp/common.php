<?php

declare(strict_types=1);

return [
    'navbar' => [
        'logout' => 'Вийти',
    ],
    'menu' => [
        'common'     => [
            'watch' => 'Переглянути',
            'add'   => 'Додати',
        ],
        'news'       => 'Новини',
        'teachers'   => 'Викладачі',
        'graduates'  => 'Випускники',
        'about'      => 'Про нас',
        'gallery'    => 'Галерея',
        'links'      => 'Корисна інформація',
        'categories' => 'Категорії',
        'contacts'   => 'Контакти',
        'profile'    => 'Профіль',
    ],
];
