<?php

declare(strict_types=1);

Route::get('/', [
    'as'   => '.index',
    'uses' => 'GraduateController@index',
]);

Route::get('/create', [
    'as'   => '.create',
    'uses' => 'GraduateController@create',
]);

Route::post('/', [
    'as'   => '.store',
    'uses' => 'GraduateController@store',
]);

Route::patch('/position', [
    'as'      => '.update.position',
    'uses'    => 'GraduateController@updatePosition',
    'laroute' => \true,
]);

Route::get('/{graduate}', [
    'as'   => '.show',
    'uses' => 'GraduateController@show',
]);

Route::get('/{graduate}/edit', [
    'as'   => '.edit',
    'uses' => 'GraduateController@edit',
]);

Route::patch('/{graduate}', [
    'as'   => '.update',
    'uses' => 'GraduateController@update',
]);

Route::delete('/{graduate}', [
    'as'   => '.destroy',
    'uses' => 'GraduateController@destroy',
]);