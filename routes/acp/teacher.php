<?php
declare(strict_types=1);

Route::get('/', [
    'as'   => '.index',
    'uses' => 'TeacherController@index',
]);

Route::get('/create', [
    'as'   => '.create',
    'uses' => 'TeacherController@create',
]);

Route::post('/', [
    'as'   => '.store',
    'uses' => 'TeacherController@store',
]);

Route::patch('/position', [
    'as'      => '.update.position',
    'uses'    => 'TeacherController@updatePosition',
    'laroute' => \true,
]);

Route::get('/{teacher}', [
    'as'   => '.show',
    'uses' => 'TeacherController@show',
]);

Route::get('/{teacher}/edit', [
    'as'   => '.edit',
    'uses' => 'TeacherController@edit',
]);

Route::patch('/{teacher}', [
    'as'   => '.update',
    'uses' => 'TeacherController@update',
]);

Route::delete('/{teacher}', [
    'as'   => '.destroy',
    'uses' => 'TeacherController@destroy',
]);

//teachers subjects
Route::get('subjects/{teacher}', [
    'as'   => '.subject.create',
    'uses' => 'SubjectController@create',
]);

Route::post('subjects/{teacher}', [
    'as'   => '.subject.store',
    'uses' => 'SubjectController@store',
]);

Route::patch('subjects/{subject}', [
    'as'      => '.subject.update',
    'uses'    => 'SubjectController@update',
    'laroute' => \true,
]);

Route::patch('subjects/position', [
    'as'      => '.subject.update.position',
    'uses'    => 'SubjectController@updatePosition',
    'laroute' => \true,
]);

Route::delete('subjects/{subject}', [
    'as'      => '.subject.destroy',
    'uses'    => 'SubjectController@destroy',
    'laroute' => \true,
]);

//teachers articles
Route::get('articles/{teacher}', [
    'as'   => '.article.create',
    'uses' => 'ArticleController@create',
]);

Route::get('articles/{paginate}/{teacher}', [
    'as'      => '.article.show',
    'uses'    => 'ArticleController@show',
    'laroute' => \true,
]);

Route::post('articles/{teacher}', [
    'as'   => '.article.store',
    'uses' => 'ArticleController@store',
]);

Route::patch('articles/{article}', [
    'as'      => '.article.update',
    'uses'    => 'ArticleController@update',
    'laroute' => \true,
]);

Route::patch('articles/position/{teacher}', [
    'as'      => '.article.update.position',
    'uses'    => 'ArticleController@updatePosition',
    'laroute' => \true,
]);

Route::delete('articles/{article}', [
    'as'      => '.article.destroy',
    'uses'    => 'ArticleController@destroy',
    'laroute' => \true,
]);