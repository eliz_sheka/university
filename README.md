### Server requirement

* PHP >=7.1.3 (extensions: php-intl, php-imagick, php-mcrypt, php-memcached, php-yaml, php-redis), 
* MySQL (>=5.7), 
* ImageMagick >=7.0, 
* Redis >=4.0, 
* Node >=9.0, 
* Yarn >=1.5

### Initial project setup

Clone repository with `mkdir /var/www/university && cd $_ && git clone https://bitbucket.org/eliz_sheka/university.git . -b master` then make a copy of example configuration file with `cp .env.example .env`

### Setting up default configuration in env file 

Make sure all the below are set to correct values.

#### Application settings

```
APP_NAME='Кафедра видавничої справи та редагування'
APP_ENV=production
APP_URL=http://university.test //put correct data
```

#### Database settings

```
DB_DATABASE="database_name"
DB_USERNAME="database_user"
DB_PASSWORD="database_password"
```

If MySQL server is not installed locally, change `DB_HOST` and `DB_PORT` to appropriate values.

#### Session settings

Driver could be either `file`, `memcached` or `redis`. Memcached or Redis are preferable options in terms of performance. 

```
SESSION_DRIVER=memcached
SESSION_DOMAIN=<PRODUCTION DOMAIN NAME>
SESSION_COOKIE_NAME=university_session
```

Please ensure that domain and cookie name are set properly in order to avoid collisions with other scripts using session on the same server

#### Cache settings

Driver could be eaither `file`, `memcached` or `redis`. Using redis is a preferable option in terms of performance.


```
CACHE_DRIVER=redis
CACHE_PREFIX=vsr_
```

Please make sure cache prefix is unique within the server, otherwise it may conflict with other scripts using redis database '0'.
In case specific settings for redis or memcached server, please refer appropriate sections in 'config/cache.php'.

### Project post-configuration

Install project dependencies. Make sure to not install development dependencies with `--no-dev` flag.

`composer install --no-dev -o`

Generate application key used for all hashing operations.

`php artisan key:generate`

Run database migrations and default seeders

`php artisan migrate --seed`

### Setting up front-end part

Install npm dependecies and compile assets

`yarn && yarn prod`


